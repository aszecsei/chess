﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : MonoBehaviour {
    public static GameData instance;

    public int level = 1;
    public List<PieceType> playerPieces;

	void Awake () {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        playerPieces = new List<PieceType>();
        playerPieces.Add(PieceType.Queen);
        playerPieces.Add(PieceType.King);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Reset()
    {
        level = 1;
        playerPieces.Clear();
        playerPieces.Add(PieceType.Queen);
        playerPieces.Add(PieceType.King);
    }
}