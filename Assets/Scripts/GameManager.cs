﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;

    public LevelGenerator levelGenerator;

    public List<ChessPiece> chessPieces;

    public GameObject availableTile;
    public GameObject cursor;
    public GameObject currentTile;

    private GameObject availableTiles;
    private GameObject cursorInstance;
    private GameObject actualCurrentTile;

    private int currentPieceIndex = 0;
    private bool isBusy = false;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Get a component reference to the attached BoardManager script
        levelGenerator = GetComponent<LevelGenerator>();

        InitGame();
    }

    //Initializes the game for each level.
    public void InitGame()
    {
        //Call the SetupScene function of the BoardManager script, pass it current level number.
        levelGenerator.SetupScene(GameData.instance.level);

        chessPieces = levelGenerator.CreatePieces(GameData.instance.level);
    }
	
	// Update is called once per frame
	void Update () {
        Camera.main.GetComponent<CameraScript>().target = chessPieces[currentPieceIndex].gameObject.transform.position;

        // Destroy all the old possible tiles
        if (availableTiles == null)
        {
            availableTiles = new GameObject();
            availableTiles.name = "Available Tiles";
        }

        int childs = availableTiles.transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(availableTiles.transform.GetChild(i).gameObject);
        }

        GameObject.Destroy(cursorInstance);

        // Show the current piece
        if(actualCurrentTile == null)
        {
            actualCurrentTile = Instantiate<GameObject>(currentTile);
        }
        actualCurrentTile.transform.position = new Vector3(chessPieces[currentPieceIndex].x * 0.64f, chessPieces[currentPieceIndex].y * 0.64f, -0.5f);

        if(!isBusy)
        {
            while (!chessPieces[currentPieceIndex].IsAlive)
            {
                currentPieceIndex = (currentPieceIndex + 1) % chessPieces.Count;
            }

            bool hasSelectedPosition = false;
            int newX = 0;
            int newY = 0;

            if (chessPieces[currentPieceIndex].IsPlayerControlled)
            {
                // Input
                // now we get the available tiles for our current piece
                List<Vector2> pCap = chessPieces[currentPieceIndex].possibleCaptures();
                List<Vector2> pMov = chessPieces[currentPieceIndex].possibleMoves();
                foreach (Vector2 pos in pCap)
                {
                    GameObject go = Instantiate<GameObject>(availableTile);
                    go.transform.parent = availableTiles.transform;
                    go.transform.position = new Vector3(pos.x * 0.64f, pos.y * 0.64f, -0.5f);
                    go.name = "Available Capture (" + pos.x + ", " + pos.y + ")";
                }
                foreach (Vector2 pos in pMov)
                {
                    GameObject go = Instantiate<GameObject>(availableTile);
                    go.transform.parent = availableTiles.transform;
                    go.transform.position = new Vector3(pos.x * 0.64f, pos.y * 0.64f, -0.5f);
                    go.name = "Available Move (" + pos.x + ", " + pos.y + ")";
                }

                // Get the closest available tile to your cursor
                cursorInstance = Instantiate<GameObject>(cursor);

                Vector3 v3 = Input.mousePosition;
                v3.z = 10.0f;
                v3 = Camera.main.ScreenToWorldPoint(v3);

                childs = availableTiles.transform.childCount;
                float minDistance = 1000000;
                Vector3 closestTilePos = Vector3.zero;
                Vector2 closestTile = Vector2.zero;
                for (int i = childs - 1; i >= 0; i--)
                {
                    if ((availableTiles.transform.GetChild(i).position - v3).sqrMagnitude < minDistance)
                    {
                        minDistance = (availableTiles.transform.GetChild(i).position - v3).sqrMagnitude;
                        closestTilePos = availableTiles.transform.GetChild(i).position;
                    }
                }

                cursorInstance.transform.position = new Vector3(closestTilePos.x, closestTilePos.y, -5);

                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 oldPos = new Vector2(chessPieces[currentPieceIndex].x, chessPieces[currentPieceIndex].y);
                    Debug.Log("(" + (closestTilePos.x / 0.64f) + ", " + (closestTilePos.y / 0.64f) + ")");
                    newX = Mathf.FloorToInt(closestTilePos.x / 0.64f + 0.2f);
                    newY = Mathf.FloorToInt(closestTilePos.y / 0.64f + 0.2f);

                    hasSelectedPosition = true;

                    // did we capture any pieces?
                    foreach (ChessPiece p in chessPieces)
                    {
                        if (!p.IsPlayerControlled && p.x == newX && p.y == newY)
                        {
                            if (p.Type == PieceType.King)
                            {
                                // Woo! next level!
                                GameData.instance.level++;
                                Application.LoadLevel(Application.loadedLevel - 1);
                            }
                            else
                            {
                                if (chessPieces[currentPieceIndex].Type == PieceType.King)
                                {
                                    p.IsPlayerControlled = true;
                                    GameData.instance.playerPieces.Add(p.Type);
                                    StartCoroutine(moveChessPiece(p, chessPieces[currentPieceIndex].x, chessPieces[currentPieceIndex].y));
                                }
                                else
                                {
                                    p.IsAlive = false;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                // Use some fancy schmancy AI

                // prefer capturing a piece
                List<Vector2> pCap = chessPieces[currentPieceIndex].possibleCaptures();
                List<Vector2> pMov = chessPieces[currentPieceIndex].possibleMoves();
                if (pCap.Count > 0)
                {
                    // pick a random possible capture
                    Vector2 cap = pCap[Random.Range(0, pCap.Count)];
                    foreach (ChessPiece other in GameManager.instance.chessPieces)
                    {
                        if (other.x == Mathf.FloorToInt(cap.x) && other.y == Mathf.FloorToInt(cap.y))
                        {
                            // TAKE IT OUT
                            Debug.Log(gameObject.name + " captured " + other.gameObject.name + " at (" + other.x + ", " + other.y + ")");
                            other.IsAlive = false;
                            GameData.instance.playerPieces.Remove(other.Type);
                        }
                    }
                    newX = Mathf.FloorToInt(cap.x + 0.2f);
                    newY = Mathf.FloorToInt(cap.y + 0.2f);
                }
                else
                {
                    if (pMov.Count > 0)
                    {
                        // pick a random possible move
                        Vector2 mov = pMov[Random.Range(0, pMov.Count)];
                        newX = Mathf.FloorToInt(mov.x + 0.2f);
                        newY = Mathf.FloorToInt(mov.y + 0.2f);
                        // Debug.Log(gameObject.name + " moved to (" + x + ", " + y + ")");
                    }
                }

                hasSelectedPosition = true;
            }

            if (hasSelectedPosition)
            {
                isBusy = true;
                StartCoroutine(moveChessPiece(chessPieces[currentPieceIndex], newX, newY));
            }
        }
	}

    IEnumerator moveChessPiece(ChessPiece piece, int x, int y)
    {
        // Wait until the camera has reached our position
        while (true)
        {
            if (!Camera.main.GetComponent<CameraScript>().hasReachedTarget)
            {
                yield return null;
            }
            else
            {
                break;
            }
        }

        if(!piece.IsPlayerControlled)
        {
            yield return new WaitForSeconds(1.0f);
        }

        Vector3 target = new Vector3(x * 0.64f, y * 0.64f, -1);

        while (true)
        {
            if ((piece.transform.position - target).sqrMagnitude > float.Epsilon)
            {
                piece.transform.position = Vector3.MoveTowards(piece.transform.position, target, 0.075f);
                yield return null;
            }
            else
            {
                break;
            }
        }

        if(!piece.IsPlayerControlled)
        {
            yield return new WaitForSeconds(1.0f);
        }

        piece.x = x;
        piece.y = y;
        piece.UpdatePosition();
        
        isBusy = false;
        currentPieceIndex = (currentPieceIndex + 1) % chessPieces.Count;
    }
}
