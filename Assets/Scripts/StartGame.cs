﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {
    public GameObject gameData;

	// Use this for initialization
	void Awake () {
        if (GameData.instance == null)
            // Instantiate GameData prefab
            Instantiate(gameData);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Select"))
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
	}
}
