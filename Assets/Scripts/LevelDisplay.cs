﻿using UnityEngine;
using System.Collections;

public class LevelDisplay : MonoBehaviour {


	// Use this for initialization
	void Start () {
	    // display some random saying!
        string saying = "";

        string[] sayings = new string[] { "\"When you see a good move, look\nfor a better one.\"", 
                                          "\"A win by an unsound combination,\nhowever showy, fills me\nwith artistic horror.\"",
                                          "\"Strategy requires thought, tactics\nrequire observation.\"",
                                          "\"Even the laziest King flees wildly\nin the face of a double check.\"",
                                          "\"Only the player with the initiative\nhas the right to attack.\"",
                                          "\"The winner of the game is the player\nwho makes the next-to-last mistake.\"",
                                          "\"You cannot play at chess if you are kind-hearted.\"",
                                          "\"The Pawns are the soul of the game.\"",
                                          "\"Some part of a mistake is always correct.\"",
                                          "\"A bad day of chess is better than\nany good day at work.\"",
                                          "\"The mistakes are there, waiting to be made.\"",
                                          "\"A player surprised is half beaten.\"",
                                          "\"Good positions don't win games, good moves do.\"",
                                          "\"No one ever won a game by resigning.\"",
                                          "\"The defensive power of a pinned\npiece is only imaginary.\"",
                                          "\"When the Chess game is over, the\nPawn and the King go back to the same box.\"",
                                          "\"One doesn't have to play well, it's\nenough to play better than your opponent.\"",
                                          "\"The hardest game to win is a won game.\"",
                                          "\"It's always better to sacrifice your opponent's men.\"",
                                          "\"To avoid losing a piece, many a person has lost the game.\"",
                                          "\"A sacrifice is best refuted by accepting it.\"",
                                          "\"Tactics flow from a superior position.\"",
                                          "\"Some sacrifices are sound; the rest are mine.\"",
                                          "\"A bad plan is better than none at all.\"",
                                          "\"On the chessboard, lies and hypocrisy do not survive long.\"",
                                          "\"It is no time to be playing chess when the house is on fire.\"",
                                          "\"Winning isn't everything...but losing is nothing.\"",
                                          "\"In chess, just as in life, today's bliss\nmay be tomorrow's poison.\"",
                                          "\"A man that will take back a move at chess will pick a pocket.\"",
                                          "\"In blitz, the Knight is stronger than the Bishop.\"",
                                          "\"A good player is always lucky.\"",
                                          "\"One bad move nullifies forty good ones.\"",
                                          "\"When you absolutely don't know what to do anymore,\nit is time to panic.\"",
                                          "\"The most powerful weapon in chess is to have the next move.\""  };

        saying = sayings[Random.Range(0, sayings.Length)];

        TextMesh t = GetComponent<TextMesh>();
        t.text = "Level: " + GameData.instance.level + "\n\n" + saying;

        StartCoroutine(nextLevel());
	}

    IEnumerator nextLevel()
    {
        yield return new WaitForSeconds(1.5f);
        AsyncOperation async = Application.LoadLevelAsync(Application.loadedLevel + 1);
        yield return async;
    }
}
