﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PieceType {
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
}

public class ChessPiece : MonoBehaviour {
    private bool _isPlayerControlled = false;
    public bool IsPlayerControlled
    {
        get
        {
            return _isPlayerControlled;
        }
        set
        {
            _isPlayerControlled = value;
            UpdateSprite();
        }
    }

    public bool NeedsUserInput
    {
        get
        {
            return _isPlayerControlled;
        }
    }

    private PieceType _type;
    public PieceType Type
    {
        get
        {
            return _type;
        }
        set
        {
            _type = value;
            UpdateSprite();
        }
    }

    private void UpdateSprite()
    {
        switch (_type)
        {
            case PieceType.Pawn:
                GetComponent<SpriteRenderer>().sprite = pawnSprite[IsPlayerControlled ? 1 : 0];
                break;
            case PieceType.Rook:
                GetComponent<SpriteRenderer>().sprite = rookSprite[IsPlayerControlled ? 1 : 0];
                break;
            case PieceType.Knight:
                GetComponent<SpriteRenderer>().sprite = knightSprite[IsPlayerControlled ? 1 : 0];
                break;
            case PieceType.Bishop:
                GetComponent<SpriteRenderer>().sprite = bishopSprite[IsPlayerControlled ? 1 : 0];
                break;
            case PieceType.Queen:
                GetComponent<SpriteRenderer>().sprite = queenSprite[IsPlayerControlled ? 1 : 0];
                break;
            case PieceType.King:
                GetComponent<SpriteRenderer>().sprite = kingSprite[IsPlayerControlled ? 1 : 0];
                break;
        }
    }

    public Sprite[] pawnSprite = new Sprite[2];
    public Sprite[] rookSprite = new Sprite[2];
    public Sprite[] knightSprite = new Sprite[2];
    public Sprite[] bishopSprite = new Sprite[2];
    public Sprite[] queenSprite = new Sprite[2];
    public Sprite[] kingSprite = new Sprite[2];

    public int x;
    public int y;

    private bool _isAlive = true;
    public bool IsAlive
    {
        get
        {
            return _isAlive;
        }
        set
        {
            _isAlive = value;
            if (!_isAlive)
            {
                if (Type == PieceType.King && IsPlayerControlled)
                {
                    // Game over, man, game over!
                    Application.LoadLevel(Application.loadedLevel + 1);
                }

                GetComponent<SpriteRenderer>().enabled = false;
                x = -100;
                y = -100;
            }
        }
    }

    void Start()
    {
        UpdateSprite();
        UpdatePosition();
    }

    public List<Vector2> possibleCaptures()
    {
        List<Vector2> result = new List<Vector2>();

        int amount;

        switch (_type)
        {
            case PieceType.Pawn:
                if (spotHasEnemyCharacter(x + 1, y + 1))
                {
                    result.Add(new Vector2(x + 1, y + 1));
                }
                if (spotHasEnemyCharacter(x - 1, y + 1))
                {
                    result.Add(new Vector2(x - 1, y + 1));
                }
                if (spotHasEnemyCharacter(x + 1, y - 1))
                {
                    result.Add(new Vector2(x + 1, y - 1));
                }
                if (spotHasEnemyCharacter(x - 1, y - 1))
                {
                    result.Add(new Vector2(x - 1, y - 1));
                }
                break;
            case PieceType.Rook:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y))
                    {
                        result.Add(new Vector2(x + amount, y));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y))
                    {
                        result.Add(new Vector2(x - amount, y));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x, y + amount))
                    {
                        result.Add(new Vector2(x, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x, y - amount))
                    {
                        result.Add(new Vector2(x, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Bishop:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y + amount))
                    {
                        result.Add(new Vector2(x + amount, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y + amount))
                    {
                        result.Add(new Vector2(x - amount, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y - amount))
                    {
                        result.Add(new Vector2(x + amount, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y - amount))
                    {
                        result.Add(new Vector2(x - amount, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Queen:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y + amount))
                    {
                        result.Add(new Vector2(x + amount, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y + amount))
                    {
                        result.Add(new Vector2(x - amount, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y - amount))
                    {
                        result.Add(new Vector2(x + amount, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y - amount))
                    {
                        result.Add(new Vector2(x - amount, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x + amount, y))
                    {
                        result.Add(new Vector2(x + amount, y));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x - amount, y))
                    {
                        result.Add(new Vector2(x - amount, y));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y + amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x, y + amount))
                    {
                        result.Add(new Vector2(x, y + amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y - amount))
                    {
                        amount++;
                    }
                    else if (spotHasEnemyCharacter(x, y - amount))
                    {
                        result.Add(new Vector2(x, y - amount));
                        amount = 1;
                        break;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Knight:
                if (spotHasEnemyCharacter(x + 1, y + 2))
                {
                    result.Add(new Vector2(x + 1, y + 2));
                }
                if (spotHasEnemyCharacter(x - 1, y + 2))
                {
                    result.Add(new Vector2(x - 1, y + 2));
                }
                if (spotHasEnemyCharacter(x + 1, y - 2))
                {
                    result.Add(new Vector2(x + 1, y - 2));
                }
                if (spotHasEnemyCharacter(x - 1, y - 2))
                {
                    result.Add(new Vector2(x - 1, y - 2));
                }
                if (spotHasEnemyCharacter(x + 2, y + 1))
                {
                    result.Add(new Vector2(x + 2, y + 1));
                }
                if (spotHasEnemyCharacter(x - 2, y + 1))
                {
                    result.Add(new Vector2(x - 2, y + 1));
                }
                if (spotHasEnemyCharacter(x + 2, y - 1))
                {
                    result.Add(new Vector2(x + 2, y - 1));
                }
                if (spotHasEnemyCharacter(x - 2, y - 1))
                {
                    result.Add(new Vector2(x - 2, y - 1));
                }
                break;
            case PieceType.King:
                if (spotHasEnemyCharacter(x + 1, y))
                {
                    result.Add(new Vector2(x + 1, y));
                }
                if (spotHasEnemyCharacter(x - 1, y))
                {
                    result.Add(new Vector2(x - 1, y));
                }
                if (spotHasEnemyCharacter(x, y + 1))
                {
                    result.Add(new Vector2(x, y + 1));
                }
                if (spotHasEnemyCharacter(x, y - 1))
                {
                    result.Add(new Vector2(x, y - 1));
                }
                if (spotHasEnemyCharacter(x + 1, y + 1))
                {
                    result.Add(new Vector2(x + 1, y + 1));
                }
                if (spotHasEnemyCharacter(x - 1, y + 1))
                {
                    result.Add(new Vector2(x - 1, y + 1));
                }
                if (spotHasEnemyCharacter(x + 1, y - 1))
                {
                    result.Add(new Vector2(x + 1, y - 1));
                }
                if (spotHasEnemyCharacter(x - 1, y - 1))
                {
                    result.Add(new Vector2(x - 1, y - 1));
                }
                break;
        }

        return result;
    }

    public List<Vector2> possibleMoves()
    {
        List<Vector2> result = new List<Vector2>();
        int amount = 1;

        switch (_type)
        {
            case PieceType.Pawn:
                if (emptySpot(x + 1, y))
                {
                    result.Add(new Vector2(x + 1, y));
                }
                if (emptySpot(x - 1, y))
                {
                    result.Add(new Vector2(x - 1, y));
                }
                if (emptySpot(x, y + 1))
                {
                    result.Add(new Vector2(x, y + 1));
                }
                if (emptySpot(x, y - 1))
                {
                    result.Add(new Vector2(x, y - 1));
                }
                break;
            case PieceType.Rook:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y))
                    {
                        result.Add(new Vector2(x + amount, y));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y))
                    {
                        result.Add(new Vector2(x - amount, y));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y + amount))
                    {
                        result.Add(new Vector2(x, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y - amount))
                    {
                        result.Add(new Vector2(x, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Bishop:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y + amount))
                    {
                        result.Add(new Vector2(x + amount, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y + amount))
                    {
                        result.Add(new Vector2(x - amount, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y - amount))
                    {
                        result.Add(new Vector2(x + amount, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y - amount))
                    {
                        result.Add(new Vector2(x - amount, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Queen:
                amount = 1;
                while (true)
                {
                    if (emptySpot(x + amount, y + amount))
                    {
                        result.Add(new Vector2(x + amount, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y + amount))
                    {
                        result.Add(new Vector2(x - amount, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y - amount))
                    {
                        result.Add(new Vector2(x + amount, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y - amount))
                    {
                        result.Add(new Vector2(x - amount, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x + amount, y))
                    {
                        result.Add(new Vector2(x + amount, y));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x - amount, y))
                    {
                        result.Add(new Vector2(x - amount, y));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y + amount))
                    {
                        result.Add(new Vector2(x, y + amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                while (true)
                {
                    if (emptySpot(x, y - amount))
                    {
                        result.Add(new Vector2(x, y - amount));
                        amount++;
                    }
                    else
                    {
                        amount = 1;
                        break;
                    }
                }
                break;
            case PieceType.Knight:
                if (emptySpot(x + 1, y + 2))
                {
                    result.Add(new Vector2(x + 1, y + 2));
                }
                if (emptySpot(x - 1, y + 2))
                {
                    result.Add(new Vector2(x - 1, y + 2));
                }
                if (emptySpot(x + 1, y - 2))
                {
                    result.Add(new Vector2(x + 1, y - 2));
                }
                if (emptySpot(x - 1, y - 2))
                {
                    result.Add(new Vector2(x - 1, y - 2));
                }
                if (emptySpot(x + 2, y + 1))
                {
                    result.Add(new Vector2(x + 2, y + 1));
                }
                if (emptySpot(x - 2, y + 1))
                {
                    result.Add(new Vector2(x - 2, y + 1));
                }
                if (emptySpot(x + 2, y - 1))
                {
                    result.Add(new Vector2(x + 2, y - 1));
                }
                if (emptySpot(x - 2, y - 1))
                {
                    result.Add(new Vector2(x - 2, y - 1));
                }
                break;
            case PieceType.King:
                if (emptySpot(x + 1, y))
                {
                    result.Add(new Vector2(x + 1, y));
                }
                if (emptySpot(x - 1, y))
                {
                    result.Add(new Vector2(x - 1, y));
                }
                if (emptySpot(x, y + 1))
                {
                    result.Add(new Vector2(x, y + 1));
                }
                if (emptySpot(x, y - 1))
                {
                    result.Add(new Vector2(x, y - 1));
                }
                if (emptySpot(x + 1, y + 1))
                {
                    result.Add(new Vector2(x + 1, y + 1));
                }
                if (emptySpot(x - 1, y + 1))
                {
                    result.Add(new Vector2(x - 1, y + 1));
                }
                if (emptySpot(x + 1, y - 1))
                {
                    result.Add(new Vector2(x + 1, y - 1));
                }
                if (emptySpot(x - 1, y - 1))
                {
                    result.Add(new Vector2(x - 1, y - 1));
                }
                break;
        }

        return result;
    }

    public void UpdatePosition()
    {
        transform.position = new Vector3(x * 0.64f, y * 0.64f, -1);
    }

    private bool emptySpot(int myX, int myY)
    {
        if (myX > 0 && myX < GameManager.instance.levelGenerator.sizeX && myY > 0 && myY < GameManager.instance.levelGenerator.sizeY)
        {
            if (GameManager.instance.levelGenerator.tileMap[myX, myY] == 1)
            {
                // check if any other pieces occupy that space
                bool works = true;
                foreach (ChessPiece other in GameManager.instance.chessPieces)
                {
                    if (other.x == myX && other.y == myY)
                    {
                        works = false;
                    }
                }
                return works;
            }
        }
        return false;
    }

    private bool spotHasEnemyCharacter(int myX, int myY)
    {
        if (myX > 0 && myX < GameManager.instance.levelGenerator.sizeX && myY > 0 && myY < GameManager.instance.levelGenerator.sizeY)
        {
            if (GameManager.instance.levelGenerator.tileMap[myX, myY] == 1)
            {
                // check if any other pieces occupy that space
                bool works = false;
                foreach (ChessPiece other in GameManager.instance.chessPieces)
                {
                    if (other.x == myX && other.y == myY)
                    {
                        works = (other.IsPlayerControlled != IsPlayerControlled);
                    }
                }
                return works;
            }
        }
        return false;
    }
}
