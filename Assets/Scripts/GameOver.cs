﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Start () {
        TextMesh m = GetComponent<TextMesh>();
        m.text = "You made it to level " + GameData.instance.level + "!\n\nPress Space to retry.";
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Select"))
        {
            GameData.instance.Reset();
            Application.LoadLevel(Application.loadedLevel - 2);
        }
	}
}
