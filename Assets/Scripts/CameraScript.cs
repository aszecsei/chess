﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    public Vector3 target = new Vector3(0, 0, 0);
    public float lerp = 0.25f;

    public bool hasReachedTarget = false;

	// Use this for initialization
	void Start () 
    {
        LevelGenerator lg = GameManager.instance.levelGenerator;
        float vertExtent = Camera.main.orthographicSize;
        float horzExtent = vertExtent * Screen.width / Screen.height;

        // Calculations assume map is in position at the origin
        minX = horzExtent;
        maxX = lg.sizeX * lg.tileSize - horzExtent;
        minY = vertExtent;
        maxY = lg.sizeY * lg.tileSize - vertExtent;
	}

    void Update()
    {
        Vector3 t3 = target;
        t3.x = Mathf.Clamp(t3.x, minX, maxX);
        t3.y = Mathf.Clamp(t3.y, minY, maxY);
        t3.z = -10;

        hasReachedTarget = ((transform.position - t3).sqrMagnitude <= 0.1f);

        Vector3 pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, target.x, lerp);
        pos.y = Mathf.Lerp(pos.y, target.y, lerp);
        transform.position = pos;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 v3 = transform.position;
        v3.x = Mathf.Clamp(v3.x, minX, maxX);
        v3.y = Mathf.Clamp(v3.y, minY, maxY);
        transform.position = v3;
	}
}
