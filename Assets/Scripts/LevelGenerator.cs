﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {
    public GameObject blackTile;
    public GameObject whiteTile;
    public int sizeX = 33;
    public int sizeY = 33;
    public float tileSize = 0.64f;
    public int randomSeed = -1;

    public int numRoomTries = 100;
    public int extraConnectorChance = 20;
    public int roomExtraSize = 0;
    public int windingPercent = 0;

    public bool isChessStyled = true;

    private List<Rect> rooms;
    private int[,] _regions;
    private int currentRegion;

    public int[,] tileMap;

    public GameObject chessPiece;

    public int initialThreat = 10;
    public int threatStep = 10;

	// Use this for initialization
	void Start () {

    }

    public void SetupScene(int level)
    {
        if (randomSeed != -1)
        {
            Random.seed = randomSeed;
        }

        Generate();

        GameObject tiles = new GameObject();
        tiles.name = "Tiles";
        tiles.transform.parent = transform;


        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                
                if(isChessStyled)
                {
                    if (x % 2 == y % 2 && tileMap[x, y] != 0)
                    {
                        GameObject tile = (GameObject)Instantiate(blackTile, new Vector3(x * tileSize, y * tileSize, transform.position.z), Quaternion.identity);
                        tile.transform.parent = tiles.transform;
                        tile.name = "Tile (" + x + ", " + y + ")";
                    }
                    else if (tileMap[x, y] != 0)
                    {
                        GameObject tile = (GameObject)Instantiate(whiteTile, new Vector3(x * tileSize, y * tileSize, transform.position.z), Quaternion.identity);
                        tile.transform.parent = tiles.transform;
                        tile.name = "Tile (" + x + ", " + y + ")";
                    }
                }
                else
                {
                    if (tileMap[x, y] == 0)
                    {
                        GameObject tile = (GameObject)Instantiate(blackTile, new Vector3(x * tileSize, y * tileSize, transform.position.z), Quaternion.identity);
                        tile.transform.parent = tiles.transform;
                        tile.name = "Tile (" + x + ", " + y + ")";
                    }
                    else if (tileMap[x, y] != 0)
                    {
                        GameObject tile = (GameObject)Instantiate(whiteTile, new Vector3(x * tileSize, y * tileSize, transform.position.z), Quaternion.identity);
                        tile.transform.parent = tiles.transform;
                        tile.name = "Tile (" + x + ", " + y + ")";
                    }
                }
            }
        }
	}

    void Generate()
    {
        tileMap = new int[sizeX, sizeY];
        _regions = new int[sizeX, sizeY];
        rooms = new List<Rect>();

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                tileMap[x, y] = 0;
                _regions[x, y] = -1;
            }
        }

        AddRooms();

        // Fill in all of the empty space with mazes
        for(int y = 1; y < sizeY; y += 2) {
            for (int x = 1; x < sizeX; x += 2)
            {
                Vector2 pos = new Vector2(x, y);
                if (tileMap[x, y] != 0) continue;
                GrowMaze(pos);
            }
        }

        ConnectRegions();
        RemoveDeadEnds();
        
        foreach(Rect room in rooms)
        {
            OnDecorateRoom(room);
        }
    }

    void AddRooms()
    {
        for(int i=0; i<numRoomTries; i++)
        {
            int size = Random.Range(1, 4 + roomExtraSize) * 2 + 1;
            int rectangularity = Random.Range(0, 2 + size / 2) * 2;
            int width = size;
            int height = size;
            if(Random.Range(0,2) > 0)
            {
                width += rectangularity;
            }
            else
            {
                height += rectangularity;
            }

            int x = Random.Range(0, (sizeX - width) / 2) * 2 + 1;
            int y = Random.Range(0, (sizeY - height) / 2) * 2 + 1;

            Rect room = new Rect(x, y, width, height);

            bool overlaps = false;
            foreach(Rect other in rooms)
            {
                if (other.Overlaps(room))
                {
                    overlaps = true;
                    break;
                }
            }

            if (overlaps) continue;

            rooms.Add(room);

            StartRegion();
            for(int myX = x; myX < x + width; myX++) {
                for(int myY = y; myY < y + height; myY++) {
                    Carve(myX, myY);
                }
            }
        }
    }

    void GrowMaze(Vector2 start)
    {
        List<Vector2> cells = new List<Vector2>();
        Vector2 lastDir = Vector2.zero;

        StartRegion();
        Carve(start);

        cells.Add(start);
        while(cells.Count != 0)
        {
            Vector2 cell = cells[cells.Count - 1];
            List<Vector2> unmadeCells = new List<Vector2>();

            Vector2[] cardinal = new Vector2[4]{new Vector2(0,1), new Vector2(0,-1), new Vector2(1,0), new Vector2(-1,0)};

            foreach(Vector2 dir in cardinal)
            {
                if (CanCarve(cell, dir)) unmadeCells.Add(dir);
            }

            if(unmadeCells.Count != 0)
            {
                // based on how "windy" passages are, try to prefer carving in the same direction
                Vector2 dir;
                if(unmadeCells.Contains(lastDir) && Random.Range(0,101) > windingPercent)
                {
                    dir = lastDir;
                }
                else
                {
                    dir = unmadeCells[Random.Range(0, unmadeCells.Count)];
                }

                Carve(cell + dir);
                Carve(cell + dir * 2);

                cells.Add(cell + dir * 2);
                lastDir = dir;
            }
            else
            {
                // No adjacent uncarved cells.
                cells.RemoveAt(cells.Count - 1);

                // This path has ended.
                lastDir = Vector2.zero;
            }
        }
    }

    void ConnectRegions()
    {
        // Find all of the tiles that can connect two (or more) regions
        Dictionary<Vector2, HashSet<int>> connectorRegions = new Dictionary<Vector2, HashSet<int>>();

        Vector2[] cardinal = new Vector2[4]{new Vector2(0,1), new Vector2(0,-1), new Vector2(1,0), new Vector2(-1,0)};
        for (int x = 1; x < sizeX - 1; x++)
        {
            for (int y = 1; y < sizeY - 1; y++)
            {
                Vector2 pos = new Vector2(x, y);
                if (tileMap[x, y] != 0) continue;

                HashSet<int> regions = new HashSet<int>();
                foreach(Vector2 dir in cardinal) {
                    int region = _regions[Mathf.FloorToInt((pos + dir).x), Mathf.FloorToInt((pos + dir).y)];
                    if (region != -1)
                    {
                        regions.Add(region);
                    }
                }

                if (regions.Count < 2) continue;

                connectorRegions[pos] = regions;
            }
        }

        List<Vector2> connectors = new List<Vector2>(connectorRegions.Keys);

        // Keep track of which regions have been merged. This maps an original
        // region index to the one it has been merged to.
        List<int> merged = new List<int>();
        HashSet<int> openRegions = new HashSet<int>();
        for (int i = 0; i <= currentRegion; i++)
        {
            merged.Add(i);
            openRegions.Add(i);
        }

        // Keep connecting regions until we're down to one.
        while (openRegions.Count > 1 && connectors.Count > 1)
        {
            Vector2 connector = connectors[Random.Range(0, connectors.Count)];

            // Carve the connection.
            AddJunction(connector);

            // Merge the connected regions. We'll pick one region (arbitrarily) and
            // map all of the other regions to its index.
            List<int> regions = new List<int>();
            foreach(int r in connectorRegions[connector])
            {
                regions.Add(merged[r]);
            }
            int dest = regions[0];
            List<int> sources = new List<int>(regions);
            sources.RemoveAt(0);

            // Merge all of the affected regions. We have to look at *all* of the
            // regions because other regions may have previously been merged with
            // some of the ones we're merging now.
            for (int i = 0; i <= currentRegion; i++)
            {
                if (sources.Contains(merged[i]))
                {
                    merged[i] = dest;
                }
            }

            // The sources are no longer in use.
            foreach (int s in sources)
            {
                openRegions.Remove(s);
            }

            int unneededConnections = 0;

            // Remove any connectors that aren't needed anymore.
            for (int i = connectors.Count - 1; i >= 0; i--)
            {
                bool shouldDelete;

                // Don't allow connectors right next to each other.
                if (Mathf.Max(Mathf.Abs(connector.x - connectors[i].x), Mathf.Abs(connector.y - connectors[i].y)) < 2)
                {
                    shouldDelete = true;
                }
                else
                {
                    // If the connector no longer spans different regions, we don't need it.
                    HashSet<int> rs = new HashSet<int>();
                    foreach (int r in connectorRegions[connectors[i]])
                    {
                        rs.Add(merged[r]);
                    }

                    if (rs.Count > 1)
                    {
                        shouldDelete = false;
                    }
                    else
                    {
                        // This connector isn't needed, but connect it occasionally so that the
                        // dungeon isn't singly-connected.
                        if (Random.Range(0, extraConnectorChance) < 1)
                        {
                            AddJunction(connectors[i]);
                        }

                        shouldDelete = true;
                        unneededConnections++;
                    }
                }

                if (shouldDelete)
                {
                    connectors.RemoveAt(i);
                }
            }
        }
    }

    void AddJunction(Vector2 pos)
    {
        tileMap[Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y)] = 1;
    }

    void RemoveDeadEnds()
    {
        Vector2[] cardinal = new Vector2[4] { new Vector2(0, 1), new Vector2(0, -1), new Vector2(1, 0), new Vector2(-1, 0) };

        bool done = false;
        while (!done)
        {
            done = true;
            for (int x = 1; x < sizeX - 1; x++)
            {
                for (int y = 1; y < sizeY - 1; y++)
                {
                    Vector2 pos = new Vector2(x, y);
                    if (tileMap[x, y] == 0) continue;

                    // If it only has one exit, it's a dead 
                    int exits = 0;
                    foreach(Vector2 dir in cardinal)
                    {
                        if (tileMap[Mathf.FloorToInt((pos + dir).x), Mathf.FloorToInt((pos + dir).y)] != 0) exits++;
                    }

                    if (exits != 1) continue;

                    done = false;
                    tileMap[x, y] = 0;
                }
            }
        }
    }

    void OnDecorateRoom(Rect room)
    {

    }

    void StartRegion()
    {
        currentRegion++;
    }

    bool CanCarve(Vector2 pos, Vector2 direction)
    {
        // Must end in bounds
        if (!(new Rect(0, 0, sizeX, sizeY)).Contains(pos + direction * 3))
        {
            return false;
        }

        // Destination must not be open.
        return (tileMap[Mathf.FloorToInt((pos + direction * 2).x), Mathf.FloorToInt((pos + direction * 2).y)] == 0);
    }

    void Carve(int x, int y)
    {
        tileMap[x, y] = 1;
        _regions[x, y] = currentRegion;
    }

    void Carve(Vector2 pos)
    {
        tileMap[Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y)] = 1;
        _regions[Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y)] = currentRegion;
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    public List<ChessPiece> CreatePieces(int level)
    {
        List<ChessPiece> result = new List<ChessPiece>();

        GameObject parent = new GameObject();
        parent.transform.position = new Vector3(0, 0, 0);
        parent.name = "Chess Pieces";

        int threat = initialThreat + (level - 1) * threatStep;

        // Instantiate player
        Rect playerRoom = rooms[Random.Range(0, rooms.Count)];
        foreach(PieceType pieceType in GameData.instance.playerPieces)
        {
            GameObject p = Instantiate<GameObject>(chessPiece);
            p.name = "Player " + pieceType.ToString();
            p.transform.parent = parent.transform;
            ChessPiece c = p.GetComponent<ChessPiece>();
            c.Type = pieceType;
            c.IsPlayerControlled = true;

            bool isDone = false;
            while (!isDone)
            {
                c.x = Random.Range(Mathf.FloorToInt(playerRoom.x), Mathf.FloorToInt(playerRoom.x + playerRoom.width));
                c.y = Random.Range(Mathf.FloorToInt(playerRoom.y), Mathf.FloorToInt(playerRoom.y + playerRoom.height));
                isDone = true;
                foreach (ChessPiece cp in result)
                {
                    if (cp.x == c.x && cp.y == c.y)
                    {
                        isDone = false;
                    }
                }
            }

            c.UpdatePosition();

            result.Add(c);
        }

        Dictionary<PieceType, int> threatRank = new Dictionary<PieceType, int>();
        threatRank.Add(PieceType.Pawn, 1);
        threatRank.Add(PieceType.Rook, 3);
        threatRank.Add(PieceType.Knight, 4);
        threatRank.Add(PieceType.Bishop, 3);
        threatRank.Add(PieceType.Queen, 6);

        bool enemiesToAdd = true;
        bool shouldAddKing = true;
        while (enemiesToAdd)
        {
            Rect r = rooms[Random.Range(0, rooms.Count)];
            if (r != playerRoom)
            {
                PieceType pt;
                if (shouldAddKing)
                {
                    shouldAddKing = false;
                    pt = PieceType.King;
                }
                else
                {
                    List<PieceType> availablePieces = new List<PieceType>();
                    foreach (PieceType key in threatRank.Keys)
                    {
                        if (threatRank[key] < threat)
                        {
                            availablePieces.Add(key);
                        }
                    }

                    if (availablePieces.Count == 0)
                    {
                        enemiesToAdd = false;
                        break;
                    }
                    else
                    {
                        pt = availablePieces[Random.Range(0, availablePieces.Count)];
                        threat -= threatRank[pt];
                    }
                }

                GameObject p = Instantiate<GameObject>(chessPiece);
                p.name = "Enemy " + pt.ToString();
                p.transform.parent = parent.transform;
                ChessPiece c = p.GetComponent<ChessPiece>();
                c.Type = pt;
                c.IsPlayerControlled = false;

                bool isDone = false;
                while (!isDone)
                {
                    c.x = Random.Range(Mathf.FloorToInt(r.x), Mathf.FloorToInt(r.x + r.width));
                    c.y = Random.Range(Mathf.FloorToInt(r.y), Mathf.FloorToInt(r.y + r.height));
                    isDone = true;
                    foreach (ChessPiece cp in result)
                    {
                        if (cp.x == c.x && cp.y == c.y)
                        {
                            isDone = false;
                        }
                    }
                }

                c.UpdatePosition();
                result.Add(c);
            }
        }

        return result;
    }
}
